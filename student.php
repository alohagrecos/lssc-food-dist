<?php
	//Check for params
	$error = false;
	
	if(empty($_GET["xid"])) {
		$error = "No student id found.";
	}

	if(!$error) {
		//Connect to SQLite DB
		try {
			$pdo = new PDO("sqlite:db/fooddistribution.sqlite");
		}
		catch(Exception $e) {
			var_dump($e->getMessage());
			die();
		}
		
		//Mark attended, if needed
		if(!empty($_GET["attended"]) && $_GET["attended"] == 1) {
			//Insert into db
			$stmt = $pdo->prepare("UPDATE students SET attended=1 WHERE xid = :xid");
			if(!$stmt->execute([':xid' => $_GET["xid"]])) {
				$error = "Error updating student record.";
			}
			$stmt = null;
		}
		
		//Search for student
		$stmt = $pdo->prepare("SELECT xid,first_name,last_name,attended FROM students WHERE xid = :xid");
		if(!$stmt->execute([':xid' => $_GET["xid"]])) {
			$error = "Error encountered searching database.";
		}
		
		$result = $stmt->fetchAll();
		$stmt = null;
		$pdo = null;
		
		//If not single result, set error
		if(count($result) > 1) {
			$error = "Multiple students found for same student id.";
		} else {
			$student = $result[0];
		}
		//If no result, set error
		if(count($result) == 0) {
			$error = "No matching student found.";
		}
	}
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="css/app-min.css?v=1" />

    <title>Student Result | Food Distribution | Lake-Sumter Community College</title>
  </head>
  <body>
    <div class="container-fluid mb-5">
      <div class="row">
        <div class="col-md-3">
          <img class="img-fluid pt-2" alt="Lake-Sumter Community College logo" src="images/LSSC_LOGOTYPE_H_PNG.png" />
        </div>
        <div class="col-md-2 stripes-bg">&nbsp;</div>
        <div class="col pt-2">
          <h3 class="text-center text-md-left">Student Assistance Emergency Programs</h3>
          <h4 class="text-center text-md-left">#LakehawkStrong</h4>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div id="search">
        <?php if($error): ?>
		<div class="row">
			<div class="col">
				<div class="alert alert-danger text-center"><?php echo $error; ?></div>
			</div>
		</div>
		<div class="row justify-content-md-center mt-2">
			<div class="col-md-6">
				<a href="./" class="btn btn-lg btn-success btn-block">Return to Search</a>
			</div>
		</div>
		<?php else: ?>
		<div class="row">
          <div class="col">
            <h2 class="text-center mb-3"><?php echo $student["last_name"]; ?>, <?php echo $student["first_name"]; ?> (<?php echo $student["xid"]; ?>)</h2>
          </div>
        </div>
		<div class="row justify-content-md-center mt-2">
			<div class="col-md-6">
				<?php if($student["attended"]): ?>
					<div class="alert alert-success text-center"><strong>Student has attended the collection event.</strong></div>
					<a href="./" class="btn btn-lg btn-success btn-block">Return to Search</a>
				<?php else: ?>
					<div class="alert alert-danger text-center"><strong>Meal not yet collected.</strong></div>
					<a href="./student.php?xid=<?php echo $student["xid"]; ?>&attended=1" class="btn btn-lg btn-success btn-block">Mark as attended</a>
					<a href="./" class="btn btn-lg btn-secondary btn-block">Return to Search</a>
				<?php endif; ?>
			</div>
		</div>
		<?php endif; ?>
      </div>
    </div>

    <script src="main.js"></script>
  </body>
</html>

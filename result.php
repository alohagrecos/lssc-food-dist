<?php
	//Check for post params
	$error = false;

	if(empty($_POST["xid"]) && empty($_POST["last_name"])) {
		$error = "No search parameters found.";
	}

	$xid = empty($_POST["xid"]) ? "none" : ("X" . $_POST["xid"]);
	$last_name = empty($_POST["last_name"]) ? "0" : $_POST["last_name"];

	if(!$error) {
		//Connect to SQLite DB
		try {
			$pdo = new PDO("sqlite:db/fooddistribution.sqlite");
		}
		catch(Exception $e) {
			var_dump($e->getMessage());
			die();
		}

		//Search for student
		$stmt = $pdo->prepare("SELECT xid,first_name,last_name,attended FROM students WHERE xid = :xid OR lower(last_name) = :last_name");
		if(!$stmt->execute([':xid' => $xid, ':last_name' => strtolower($last_name)])) {
			$error = "Error encountered searching database.";
		}

		$result = $stmt->fetchAll();
		$stmt = null;
		$pdo = null;

		//If single result, send to single result page
		if(count($result) == 1) {
			header("Location:./student.php?xid=" . $result[0]["xid"]);
			die();
		}
		//If no result, set error
		if(count($result) == 0) {
			$error = "No matching student found.";
		}
	}
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="css/app-min.css?v=1" />

    <title>Search Result | Food Distribution | Lake-Sumter Community College</title>
  </head>
  <body>
    <div class="container-fluid mb-5">
      <div class="row">
        <div class="col-md-3">
          <img class="img-fluid pt-2" alt="Lake-Sumter Community College logo" src="images/LSSC_LOGOTYPE_H_PNG.png" />
        </div>
        <div class="col-md-2 stripes-bg">&nbsp;</div>
        <div class="col pt-2">
          <h3 class="text-center text-md-left">Student Assistance Emergency Programs</h3>
          <h4 class="text-center text-md-left">#LakehawkStrong</h4>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div id="search">
        <div class="row">
          <div class="col">
            <h2 class="text-center mb-3">Search Results:</h2>
          </div>
        </div>
        <?php if($error): ?>
		<div class="row">
			<div class="col">
				<div class="alert alert-danger text-center"><?php echo $error; ?></div>
			</div>
		</div>
		<div class="row justify-content-md-center mt-2">
			<div class="col-md-6">
				<a href="./" class="btn btn-lg btn-success btn-block">Return to Search</a>
			</div>
		</div>
		<?php else: ?>
		<div class="row">
			<div class="col">
				<div class="alert alert-success text-center">Multiple results found, please select the correct student below</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<table class="table table-striped">
					<thead><tr><th>XID</th><th>First Name</th><th>Last Name</th><th></th></tr></thead>
					<tbody>
						<?php foreach($result as $student): ?>
						<tr><td class="align-middle"><?php echo $student["xid"]; ?></td><td class="align-middle"><?php echo $student["first_name"]; ?></td><td class="align-middle"><?php echo $student["last_name"]; ?></td><td class="align-middle"><a href="student.php?xid=<?php echo $student["xid"]; ?>" class="btn btn-lg btn-block btn-success">Select</a></td></tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row justify-content-md-center mt-2">
			<div class="col-md-6">
				<a href="./" class="btn btn-lg btn-success btn-block">Return to Search</a>
			</div>
		</div>
		<?php endif; ?>
      </div>
    </div>

    <script src="main.js"></script>
  </body>
</html>

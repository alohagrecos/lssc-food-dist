<?php
//=====================
// Handle upload
//=====================
if(isset($_POST["submit"])) {
  if(isset($_FILES["file"])) {
    //if there was an error uploading the file
    if($_FILES["file"]["error"] > 0) {
      echo "Error - Return Code: " . $_FILES["file"]["error"] . "<br />";
    }
    else {
      //Store the file
      move_uploaded_file($_FILES["file"]["tmp_name"], "tmp/input.csv");
      //Connect to SQLite DB
      try {
        $pdo = new PDO("sqlite:db/fooddistribution.sqlite");
      }
      catch(Exception $e) {
        var_dump($e->getMessage());
		die();
      }
	  //Clear the DB
	  $pdo->exec("DELETE FROM students WHERE 1=1;");

      //========================
      // Parse input
      //========================
      $row = 1;
      if(($handle = fopen("tmp/input.csv", "r")) !== FALSE) {
        while(($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
          if($data[0] == "xid") {
            continue;
          }

          if(count($data) > 3 || count($data) < 3) {
            echo "Incorrect field count in input file on row $row <br>";
            break;
          }

          //Insert into db
          $stmt = $pdo->prepare("INSERT INTO students (xid, first_name, last_name) VALUES(:xid, :first_name, :last_name)");
		  if(!$stmt->execute([':xid' => $data[0], ':first_name' => $data[1], ':last_name' => $data[2]])) {
            echo "Error inserting row $row into database<br>";
            break;
          }
          $stmt = null;

          $row++;
        }
        fclose($handle);
		$pdo = null;
        unlink("tmp/input.csv");
      } else {
        echo "Could not read input file.";
      }
    }
  }
}
?>
<html>
<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" enctype="multipart/form-data">
<p>Select source CSV data file. Should have the following fields, including header:<br>xid, first_name, last_name<br><br><b>Warning: this will delete all existing records.</b></p>
<p><input type="file" name="file" id="file" /></p>
<p><input type="submit" name="submit" /></p>
</form>
</html>

# Lake-Sumter State College Emergency Assistance Programs
## Food Distribution tracker

### Requirements
1. PHP 5+
2. pdo sql lite extension enabled in your php.ini

### Setup
1. Copy all files from this repository to your web server
2. Ensure the "db" directory copied to your web server is writable by the user account running php (for example, IUSR is default user on IIS installations)
3. Navigate to <your site>/<directory where files are>/setup.php
4. Complete the CSV upload to populate your database

### Use
1. Navigate to <your site>/<directory where files are>
2. Search for any user
